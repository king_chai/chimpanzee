#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: king_chai
# Date: 2018-12-07

from pyspark import HiveContext, SparkConf
from pyspark import SparkContext

reload(sys)
sys.setdefaultencoding('utf8')


def main(sqlContext):
    pass


if __name__ == '__main__':
    sc = SparkContext(appName='XXXX')
    sc.setLogLevel("WARN")
    sqlContext = HiveContext(sc)
    main(sqlContext)
    print "!XXXX Done!"
    sc.stop()
