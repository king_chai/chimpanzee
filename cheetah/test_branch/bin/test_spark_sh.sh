#!/usr/bin/env bash
#set ENV
source /etc/profile
unset SPARK_HOME
unset SPARK_JAR
export SPARK_HOME=/opt/spark-2.1.0-bin-hadoop2.6
#export HADOOP_CONF_DIR=/opt/hadoop-2.6.0/etc/hadoop
export HADOOP_CONF_DIR=/etc/hadoop/conf
export PYSPARK_PYTHON=/usr/bin/python
export PYSPARK_DRIVER_PYTHON=/usr/bin/python
export PYTHONPATH=$SPARK_HOME/python:$SPARK_HOME/python/py4j-0.10.3-src.zip

export ONLINE_WS=~/online/tesla
cd $ONLINE_WS

$SPARK_HOME/bin/spark-submit  \
--master yarn   \
--deploy-mode client \
--executor-memory 15G \
--executor-cores 4 \
--driver-memory 3g  \
--name user_status \
--conf spark.yarn.executor.memoryOverhead=1024 \
--conf spark.dynamicAllocation.minExecutors=2 \
--conf spark.dynamicAllocation.maxExecutors=10 \
--conf spark.dynamicAllocation.enabled=true \
--conf spark.shuffle.service.enabled=true \
--py-files  /home/core_adm/online/tesla/tesla.zip   \
--jars ${ONLINE_WS}/tesla/lib/mysql.jar \
chimpanzee/cheetah/test_branch/test_spark_code.py \
"$@"




