#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@File  : def_summary.py
@Author: king_chai
@Date  : 2018/12/7 15:28
@Desc  :
'''
import re


def conect_mysql():
    # 1、链接mysql
    import MySQLdb

    conn = MySQLdb.connect(
        host='10.0.10.135',
        port=3306,
        user='xman',
        passwd='XnRm3xCOSlx6hLyj',
        db='db_qxb_newer',
    )
    cur = conn.cursor()
    cur.execute('set names utf8')
    cur.execute('select * from test')

    d = cur.fetchall()
    if '所选字段中包含中文':
        dd = [[j.decode("utf8") for j in i] for i in d]

    # 2、spark中读取mysql数据
    dbtable = 'db_business.t_administrative_punishment'
    conf = {
        "driver": "com.mysql.jdbc.Driver",
        "url": "jdbc:mysql://10.0.4.124:3306",
        "dbtable": dbtable,
        "user": "u_temp",
        "password": "yi2gaunaEAfKREYR",
        "batchsize": "1000",
    }
    df = spark.read.format("jdbc").options(**conf).load()


def get_name(row):
    '''
    :param row: pending field
    :return: cleaned string
    '''
    if row:
        regex = re.compile(ur"[\u4e00-\u9fa5A-Za-z0-9]+")
        list1 = re.findall(regex, row)
        return ''.join(list1)
    else:
        return ''
